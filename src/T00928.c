#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    int n, min = +100, max = -100, digit;
    scanf("%d", &n);

    for (int i = 0; i < n; i++)
    {
        scanf("%d", &digit);

        min = (digit < min) ? digit : min;
        max = (digit > max) ? digit : max;
    }

    printf("%d\n", min + max);
    return EXIT_SUCCESS;
}
