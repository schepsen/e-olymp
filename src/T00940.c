#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    int x, y;
} point_t;

#include <math.h>

double len(point_t* p1, point_t* p2)
{
    return sqrt(pow(p1->x - p2->x, 2.0) + pow(p1->y - p2->y, 2.0));
}

double area(point_t* p1, point_t* p2, point_t* p3)
{
    double a = len(p2, p3);
    double b = len(p1, p3);
    double c = len(p1, p2);

    double s = 0.5 * (a + b + c);

    return sqrt(s * (s - a) * (s - b) * (s - c));
}

int main(int argc, char** argv)
{
    point_t a, b, c;
    scanf("%d %d %d %d %d %d", &a.x, &a.y, &b.x, &b.y, &c.x, &c.y);

    printf("%0.1lf\n", area(&a, &b, &c));
    return EXIT_SUCCESS;
}
