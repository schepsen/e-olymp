#include <stdio.h>
#include <stdlib.h>

#include <math.h>

int main(int argc, char** argv)
{
    unsigned int d, p;

    scanf("%u %u", &d, &p);

    double s = p + 0.5 * d;
    double area = 4 * (sqrt(s * (s - d) * pow((s - p), 2))) + pow(d, 2);

    double vol = (pow(d, 2) * sqrt(pow(p, 2) - pow(sqrt(0.5 * pow(d, 2)), 2))) / 3.0;
    printf("%0.3lf %0.3lf\n", area, vol);

    return EXIT_SUCCESS;
}
