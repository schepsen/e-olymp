#include <stdio.h>
#include <stdlib.h>

#include <math.h>

int main(int argc, char ** argv)
{
    int n;
    scanf("%d", &n);

    double numbers[n], max;

    for (int i = 0; i < n; i++)
    {
        scanf("%lf", &numbers[i]);
    }

    max = numbers[0];

    for (int i = 1; i < n; i++)
    {
        if (numbers[i] > max)
            max = numbers[i];
    }

    printf("%.2lf\n", fabs(max));
    return EXIT_SUCCESS;
}
