#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    int x, y;
} point_t;

#include <math.h>

double len(point_t* p1, point_t* p2)
{
    return sqrt(pow(p1->x - p2->x, 2.0) + pow(p1->y - p2->y, 2.0));
}

int main(int argc, char** argv)
{
    point_t a, b, c, d;
    scanf("%d %d\n%d %d\n%d %d\n%d %d", &a.x, &a.y, &b.x, &b.y, &c.x, &c.y, &d.x, &d.y);

    double ac = len(&a, &c);
    double bd = len(&b, &d);

    printf("%0.3lf %0.3lf\n%0.3lf %0.3lf\n", 0.5 * (a.x + c.x), 0.5 * (a.y + c.y), ac, bd);
    return EXIT_SUCCESS;
}
