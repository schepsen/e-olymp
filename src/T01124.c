#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    unsigned int n;
    scanf("%d", &n);

    for (int i = 1; i <= n; i++)
    {
        printf("%c", 'a');
        for (int j = n; j > 0; j--)
        {
            if ((j - i) > 0)
            {
                printf(" ");
            }
            else
            {
                printf("%c", 'a' + (-1 * (j - i)));
            }
        }
        printf("\n");
    }
    return EXIT_SUCCESS;
}
