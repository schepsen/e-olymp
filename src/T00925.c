#include <math.h>

#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    double x, y;
} point_t;

double len(point_t* p1, point_t* p2)
{
    return pow(pow((p1->x - p2->x), 2.0) + pow((p1->y - p2->y), 2.0), 0.5);
}

int main(int argc, char** argv)
{
    point_t a, b, c;
    scanf("%lf %lf %lf %lf %lf %lf", &a.x, &a.y, &b.x, &b.y, &c.x, &c.y);

    double side_a = len(&b, &c), side_b = len(&a, &c), side_c = len(&a, &b);

    double perimeter = side_a + side_b + side_c, sp = 0.5 * perimeter;
    double area = pow(sp * (sp - side_a) * (sp - side_b) * (sp - side_c), 0.5);

    printf("%0.4lf %0.4lf\n", perimeter, area);
    return EXIT_SUCCESS;
}
