#include <stdio.h>
#include <stdlib.h>

#include <string.h>

int main(int argc, char ** argv) {
  char str[250];
  int flag = 0, sum = 0;
  scanf("%s", str);

  for (int i = 0; i < strlen(str); i++)
  {
    if (str[i] == '+' || str[i] == '-' || str[i] == '*')
    {
      if (flag)
      {
        sum++;
        flag = 0;
      }
    } else
    {
      flag = 1;
    }
  }

  printf("%d\n", sum);

  return EXIT_SUCCESS;
}
