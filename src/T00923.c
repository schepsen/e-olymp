#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    unsigned int month;
    scanf("%d", &month);

    char* season[4] = { "Winter", "Spring", "Summer", "Autumn" };

    printf("%s\n", season[((month % 12) / 3)]);
    return EXIT_SUCCESS;
}
