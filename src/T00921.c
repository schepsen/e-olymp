#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    unsigned int n, count = 0;
    scanf("%d", &n);
    double number, sum = 0.0;

    for (int i = 0; i < n; i++)
    {
        scanf("%lf", &number);
        if (number < 0)
        {
            count++;
            sum += number;
        }
    }

    printf("%d %0.2f\n", count, sum);
    return EXIT_SUCCESS;
}
