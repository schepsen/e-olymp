#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
    int n;
    scanf("%d", &n);

    double numbers[n], min;

    for (int i = 0; i < n; i++)
    {
        scanf("%lf", &numbers[i]);
    }

    min = numbers[0];

    for (int i = 1; i < n; i++)
    {
        if (numbers[i] < min)
            min = numbers[i];
    }

    printf("%.2lf\n", min * 2);
    return EXIT_SUCCESS;
}
