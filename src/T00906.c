#include <stdio.h>
#include <stdlib.h>

int convert(char c)
{
    return c - '0';
}

int main(int argc, char ** argv)
{
    char string[3];
    scanf("%s", string);

    printf("%d\n", convert(string[0]) * convert(string[1]) * convert(string[2]));

    return EXIT_SUCCESS;
}
