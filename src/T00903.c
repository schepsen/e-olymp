#include <stdio.h>
#include <stdlib.h>

#define MAX(x,y) (((x)>(y))?(x):(y))

int main(int argc, char ** argv)
{
    char a, b, c;
    scanf("%c%c%c", &a, &b, &c);

    if (a == c)
    {
        printf("=\n");
    }
    else
    {
        printf("%c\n", MAX(a, c));
    }

    return EXIT_SUCCESS;
}
