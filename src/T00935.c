#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    unsigned int index;
    char stream[4];

    scanf("%s", stream);
    index = (stream[0] != '-') ? 0 : 1;

    printf("%d\n%d\n%d\n", stream[index] - '0', stream[index + 1] - '0', stream[index + 2] - '0');
    return EXIT_SUCCESS;
}
