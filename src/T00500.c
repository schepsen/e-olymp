#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
    int i, n;
    scanf("%d", &n);

    int offers[n][3];

    for (i = 0; i < n; i++)
    {
        scanf("%d %d %d", &offers[i][0], &offers[i][1], &offers[i][2]);
    }

    for (i = 0; i < n; i++)
    {
        printf("%d\n", ((((2 * offers[i][2]) * (offers[i][0] + offers[i][1])) - 1) / 16) + 1);
    }
    return EXIT_SUCCESS;
}
