#include <stdio.h>
#include <stdlib.h>

double max(double x, double y)
{
    return (x > y) ? x : y;
}

double min(double x, double y, double z)
{
    if (x <= y)
    {
        if (x < z)
        {
            return x;
        }
    }
    else if (y <= x)
    {
        if (y < z)
        {
            return y;
        }
    }
    return z;
}

int main(int argc, char ** argv)
{

    double x, y, z;
    scanf("%lf %lf %lf", &x, &y, &z);

    printf("%.2lf\n", min(max(x, y), max(y, z), x + y + z));
    return EXIT_SUCCESS;
}
