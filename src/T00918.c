#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    double x, y;
    scanf("%lf %lf", &x, &y);

    printf("%d\n", (x == 0 | y == 0) ? 0 : (x < 0) ? ((y < 0) ? 3 : 2) : ((y < 0) ? 4 : 1));
    return EXIT_SUCCESS;
}
