#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    unsigned int t, n, m;
    scanf("%d", &t);

    for (int i = 0; i < t; i++)
    {
        scanf("%d %d", &n, &m);
        printf("%d", ((n % (m + 1)) != 0) ? 1 : 2);
    }

    printf("%s", "\n");
    return EXIT_SUCCESS;
}
