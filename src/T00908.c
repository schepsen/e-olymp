#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
    int n, sum = 0, c = 0;
    scanf("%d", &n);

    int numbers[n];

    for (int i = 0; i < n; i++)
    {
        scanf("%d", &numbers[i]);
    }

    for (int i = 0; i < n; i++)
    {
        if ((numbers[i] > 0) && ((numbers[i] % 6) == 0))
        {
            sum += numbers[i];
            c++;
        }
    }

    printf("%d %d\n", c, sum);

    return EXIT_SUCCESS;
}
