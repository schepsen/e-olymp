#include <stdio.h>
#include <stdlib.h>

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

int main(int argc, char ** argv)
{
  int a, b, c, d, variants = 0, ans, *numbers = (int *) calloc(101, sizeof(int));
  scanf("%d %d %d %d", &a, &b, &c, &d);

  for (int i = (min(a, b)); i <= (max(a, b)); i++)
  {
    for (int j = (min(c, d)); j <= (max(c, d)); j++)
    {
      ans = i * j;
      if (!numbers[ans])
      {
        variants++;
        numbers[ans] = 1;
      }
    }
  }
  printf("%d\n", variants);
  return EXIT_SUCCESS;
}
