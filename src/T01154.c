#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    unsigned int n, m, idx = 0, *items, flag;
    while (scanf("%d %d", &n, &m) != EOF)
    {
        items = (unsigned int *) calloc(n, sizeof(unsigned int));
        flag = 1;

        while (items[idx = (idx + m) % n] == 0)
        {
            items[idx] = 1;
        }

        for (int i = 0; i < n; i++)
        {
            if (items[i] == 0)
            {
                flag = 0;
                break;
            }
        }
        printf((flag == 1) ? "YES\n" : "NO\n");
        free(items);
    }

    return EXIT_SUCCESS;
}
