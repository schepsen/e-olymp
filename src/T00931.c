#include <string.h>

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    unsigned int product = 1, sum = 0;
    char stream[20];

    scanf("%[0-9\n]", stream);

    for (int i = 0; i < strlen(stream); i++)
    {
        sum += stream[i] - '0';
        product *= stream[i] - '0';
    }

    printf("%.3lf\n", (double) product / sum);
    return EXIT_SUCCESS;
}
