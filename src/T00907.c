#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
    int n;
    scanf("%d", &n);

    float numbers[n];

    for (int i = 0; i < n; i++)
    {
        scanf("%f", &numbers[i]);
    }

    for (int i = 0; i < n; i++)
    {
        if (numbers[i] <= 2.5)
        {
            printf("%d %.2f\n", i + 1, numbers[i]);
            return EXIT_SUCCESS;
        }
    }

    printf("Not Found\n");

    return EXIT_SUCCESS;
}
