#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    unsigned int n, combinations;

    scanf("%u", &n);

    if (n == 0 || n == 1 || n == 2)
    {
        combinations = n;
    }
    else
    {
        combinations = n * (n - 1) * (n - 2);
    }

    printf("%u\n", combinations);
    return EXIT_SUCCESS;
}
