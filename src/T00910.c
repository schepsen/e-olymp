#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
    int n, counter = 0;
    scanf("%d", &n);

    float sum = 0.0, numbers[n];

    for (int i = 0; i < n; i++)
    {
        scanf("%f", &numbers[i]);
    }

    for (int i = 0; i < n; i++)
    {
        if (numbers[i] > 0)
        {
            counter++;
            sum += numbers[i];
        }
    }

    if (counter)
    {
        printf("%.2f\n", (sum / counter));
    }
    else
    {
        printf("Not Found\n");
    }

    return EXIT_SUCCESS;
}
