#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
    int id;

    scanf("%d", &id);
    char desc[4][11] = { "Initial", "Average", "Sufficient", "High" };

    printf("%s\n", desc[(id - 1) / 3]);

    return EXIT_SUCCESS;
}
