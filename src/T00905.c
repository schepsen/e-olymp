#include <stdio.h>
#include <stdlib.h>

int getType(int a, int b, int c)
{
    if ((a == b) && (b == c))
    {
        return 1;
    }
    if (((a == b) && (b != c)) || ((a == c) && (b != c))
            || ((b == c) && (a != c)))
    {
        return 2;
    }

    return 3;
}

int main(int argc, char ** argv)
{
    int a, b, c;
    scanf("%d %d %d", &a, &b, &c);
    printf("%d\n", getType(a, b, c));

    return EXIT_SUCCESS;
}
