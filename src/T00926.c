#include <stdio.h>
#include <stdlib.h>

#include <math.h>

double area(double a, double b, double c, double s)
{
    return pow(s * (s - a) * (s - b) * (s - c), 0.5);
}

int main(int argc, char** argv)
{
    double a, b, c, d, f;
    scanf("%lf %lf %lf %lf %lf", &a, &b, &c, &d, &f);

    printf("%0.4lf\n", area(a, b, f, 0.5 * (a + b + f)) + area(c, d, f, 0.5 * (c + d + f)));
    return EXIT_SUCCESS;
}
