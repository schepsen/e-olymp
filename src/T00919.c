#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{

    double sum = 0.0f;
    int n, count = 0;
    scanf("%d", &n);
    double array[n + 1];

    for (int i = 1; i <= n; i++)
    {
        scanf("%lf", &array[i]);
        if (((i % 3) == 0) && (array[i] > 0))
        {
            sum += array[i];
            count++;
        }
    }

    printf("%d %.2lf\n", count, sum);

    return EXIT_SUCCESS;
}
