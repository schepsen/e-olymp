#include <stdio.h>
#include <stdlib.h>

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

#include <math.h>

int main(int argc, char** argv)
{
    int a, b, c;
    scanf("%d %d %d", &a, &b, &c);

    double D = pow(b, 2.0) - (4 * a * c);
    if (D > 0)
    {
        int x1 = (-b + sqrt(D)) / (2 * a);
        int x2 = (-b - sqrt(D)) / (2 * a);

        printf("Two roots: %d %d\n", min(x1, x2), max(x1, x2));
    }
    else if (D < 0)
    {
        printf("No roots\n");

    }
    else
    {
        printf("One root: %d\n", -(b / (2 * a)));
    }

    return EXIT_SUCCESS;
}
