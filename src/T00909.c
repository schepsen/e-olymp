#include <string.h>

#include <stdio.h>
#include <stdlib.h>

#define SIZE 250

int main(int argc, char ** argv)
{
    char string[SIZE];
    fgets(string, SIZE, stdin);

    int c = 1;

    for (int i = 0; i < strlen(string); i++)
    {
        if (string[i] == ' ')
        {
            c++;
        }
    }

    printf("%d\n", c);
    return EXIT_SUCCESS;
}
