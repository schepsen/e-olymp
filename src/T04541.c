#include <string.h>

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
  unsigned long year, cost;
  int c = 1;
  scanf("%lu %lu", &year, &cost);

  char string[16];
  sprintf(string, "%lu", year);

  for (int i = (strlen(string) - 1); i >= 0; i--)
  {
    if (string[i] == '9')
    {
      c++;
      continue;
    }
    else
    {
      if ((string[i] == '8') || (string[i] == '0'))
      {
        if (string[i + 1] == '9')
        {
          c--;
        }
      }
      break;
    }
  }
  printf("%lu\n", (unsigned long) cost * c);
  return EXIT_SUCCESS;
}
