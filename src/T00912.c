#include <stdio.h>
#include <stdlib.h>

#include <string.h>

int main(int argc, char ** argv)
{
    char line[251];
    fgets(line, 250, stdin);

    int sum = 0, combination = 0;
    for (int i = 0; i < strlen(line); i++)
    {
        switch (line[i])
        {
        case '!':
        case '.':
        case '?':
            if (!combination)
            {
                sum++;
            }
            combination = 1;
            break;
        default:
            combination = 0;
            break;
        }
    }
    printf("%d\n", sum);

    return EXIT_SUCCESS;
}
