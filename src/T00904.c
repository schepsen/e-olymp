#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
    int n;
    scanf("%d", &n);

    int list[n];

    for (int i = 0; i < n; i++)
    {
        scanf("%d", &list[i]);
    }

    for (int i = 0; i < n - 1; i++)
    {
        if (list[i] >= 0)
        {
            printf("%d ", list[i] + 2);
        }
        else
        {
            printf("%d ", list[i]);
        }
    }

    if (list[n - 1] >= 0)
    {
        printf("%d\n", list[n - 1] + 2);
    }
    else
    {
        printf("%d\n", list[n - 1]);
    }

    return EXIT_SUCCESS;
}
