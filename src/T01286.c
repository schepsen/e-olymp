#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    unsigned int a, b, c;

    scanf("%u %u %u", &a, &b, &c);

    printf("%u\n", a + b + c);
    return EXIT_SUCCESS;
}
