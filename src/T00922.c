#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    unsigned int n;
    scanf("%d", &n);

    int items[n];

    for (int i = 0; i < n; i++)
    {
        scanf("%d", items + ((i + 1) % n));
    }
    for (int i = 0; i < n - 1; i++)
    {
        printf("%d ", items[i]);
    }

    printf("%d\n", items[n - 1]);
    return EXIT_SUCCESS;
}
