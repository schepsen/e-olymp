#include <stdio.h>
#include <stdlib.h>

#include <string.h>

int main(int argc, char ** argv) {
  char buffer[10];
  unsigned int x;

  scanf("%d", &x);
  sprintf(buffer, "%d", x);

  printf("%d\n", (int) strlen(buffer));
  return EXIT_SUCCESS;
}
