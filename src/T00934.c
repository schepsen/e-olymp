#include <stdio.h>
#include <stdlib.h>

#include <math.h>

double height(unsigned int a, unsigned int b, unsigned int c)
{
    double p = 0.5 * (a + b + c);

    return ((double) 2 / a) * sqrt(p * (p - a) * (p - b) * (p - c));
}

int main(int argc, char** argv)
{
    unsigned int a, b, c;

    scanf("%u %u %u", &a, &b, &c);

    printf("%.2lf %.2lf %.2lf\n", height(a, b, c), height(b, a, c), height(c, a, b));
    return EXIT_SUCCESS;
}
