#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
    int x;

    scanf("%d", &x);
    printf("%d %d\n", (x / 10), (x % 10));

    return EXIT_SUCCESS;
}
