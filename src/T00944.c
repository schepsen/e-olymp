#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    int x, y;
} point_t;

#include <math.h>

double len(point_t* p1, point_t* p2)
{
    return sqrt(pow(p2->x - p1->x, 2.0) + pow(p2->y - p1->y, 2.0));
}

double area(point_t* p1, point_t* p2, point_t* p3)
{
    double a = len(p2, p3);
    double b = len(p1, p3);
    double c = len(p1, p2);

    double s = 0.5 * (a + b + c);

    return sqrt(s * (s - a) * (s - b) * (s - c));
}

int main(int argc, char** argv)
{
    point_t a, b, c, d;

    scanf("%d %d %d %d %d %d %d %d", &a.x, &a.y, &b.x, &b.y, &c.x, &c.y, &d.x, &d.y);

    printf("%0.0lf\n", ceil(area(&a, &b, &d) + area(&b, &c, &d)));
    return EXIT_SUCCESS;
}
