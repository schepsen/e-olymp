#include <stdio.h>
#include <stdlib.h>

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

#include <math.h>

int cmp(const void *a, const void *b)
{
    return ((*(int *) a) - (*(int *) b));
}

int main(int argc, char ** argv)
{
    int numbers[3];
    scanf("%d %d %d", &numbers[0], &numbers[1], &numbers[2]);

    qsort(numbers, 3, sizeof(int), &cmp);

    if ((pow(numbers[0], 2.0) + pow(numbers[1], 2.0)) == pow(numbers[2], 2.0))
    {
        printf("YES\n");
    }
    else
    {
        printf("NO\n");
    }

    return EXIT_SUCCESS;
}
