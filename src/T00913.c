#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
    int n;
    scanf("%d", &n);

    double numbers[n][2];

    for (int i = 0; i < n; i++)
    {
        scanf("%lf %lf", &numbers[i][0], &numbers[i][1]);
    }

    for (int i = 0; i < n; i++)
    {
        printf("%.4lf %.4lf\n", numbers[i][0] + numbers[i][1], numbers[i][0] * numbers[i][1]);
    }

    return EXIT_SUCCESS;
}
