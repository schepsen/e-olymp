#include <string.h>

#include <stdio.h>
#include <stdlib.h>

void append(char* string, int digit)
{
    char temp[20] = "";
    if (strlen(string) == 0)
    {
        sprintf(temp, "%d", digit);
    }
    else
    {
        sprintf(temp, "%s %d", string, digit);
    }
    sprintf(string, "%s", temp);
}

int main(int argc, char** argv)
{
    int mask[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, amount = 0, index;
    char stream[30], result[30] = "";

    scanf("%[^\n]", stream);

    for (int i = 0; i < strlen(stream); i++)
    {
        if ((index = stream[i] - '0') < 10)
        {
            mask[index] += 1;
        }
    }

    for (int i = 0; i < 10; i++)
    {
        if (mask[i] == 0)
        {
            amount += 1;
            append(result, i);
        }
    }

    if (amount == 0)
    {
        printf("%d\n", amount);
    }
    else
    {
        printf("%d\n%s\n", amount, result);
    }

    return EXIT_SUCCESS;
}
