#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    int n, amount, count = 0;
    double price;

    scanf("%d", &n);

    for (int i = 0; i < n; i++)
    {
        scanf("%d %lf", &amount, &price);
        if (price < 50.0)
        {
            count += amount;
        }
    }

    printf("%d\n", count);
    return EXIT_SUCCESS;
}
