#include <stdio.h>
#include <stdlib.h>

#include <math.h>

int main(int argc, char** argv)
{
    int s, a;
    scanf("%d %d", &s, &a);

    double height = -(0.5 * a) + pow(0.25 * (a * a) + 2 * s, 0.5);

    printf("%.2lf\n", height);
    return EXIT_SUCCESS;
}
